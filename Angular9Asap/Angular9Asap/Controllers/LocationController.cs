﻿using System.Collections.Generic;
using System.Linq;
using Angular9Asap.Context;
using Microsoft.AspNetCore.Mvc;
using Models.Location;

namespace Angular9Asap.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly PersonContext locationDetails;
        public LocationController(PersonContext personContext)
        {
            locationDetails = personContext;
        }

        [HttpGet]
        public IEnumerable<Location> Get()
        {
            var data = locationDetails.Location.ToList();
            return data;
        }

        [HttpPost]
        public IActionResult Post([FromBody] Location obj)
        {
            var data = locationDetails.Location.Add(obj);
            locationDetails.SaveChanges();
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Location obj)
        {
            var data = locationDetails.Location.Update(obj);
            locationDetails.SaveChanges();
            return Ok();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var data = locationDetails.Location.Where(a => a.Id == id).FirstOrDefault();
            locationDetails.Location.Remove(data);
            locationDetails.SaveChanges();
            return Ok();

        }
    }
}
