﻿using System.Collections.Generic;
using System.Linq;
using Angular9Asap.Context;
using Microsoft.AspNetCore.Mvc;
using Models.Person;

namespace Angular9Asap.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly PersonContext personDetails;
        public PersonController(PersonContext personContext)
        {
            personDetails = personContext;
        }

        [HttpGet]
        public IEnumerable<Person> Get()
        {
            var data = personDetails.Person.ToList();
            return data;
        }

        [HttpPost]
        public IActionResult Post([FromBody] Person obj)
        {
            personDetails.Person.Add(obj);
            personDetails.SaveChanges();
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Person obj)
        {
            personDetails.Person.Update(obj);
            personDetails.SaveChanges();
            return Ok();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var data = personDetails.Person.Where(a => a.Id == id).FirstOrDefault();
            personDetails.Person.Remove(data);
            personDetails.SaveChanges();
            return Ok();
        }
    }
}
