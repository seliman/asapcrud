﻿using Microsoft.EntityFrameworkCore;
using Models.Location;
using Models.Person;
using System;

namespace Angular9Asap.Context
{
    public class PersonContext : DbContext
    {
        public PersonContext(DbContextOptions<PersonContext> options) : base(options)
        {
        }
        public DbSet<Person> Person { get; set; }
        public DbSet<Location> Location { get; set; }
    }
}