﻿
using System.ComponentModel.DataAnnotations;

namespace Models.Person
{
    public class Person
    {
        [Key]
        public int? Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string DetailedAddress { get; set; }
        public int? LocationId { get; set; }
    }
}
