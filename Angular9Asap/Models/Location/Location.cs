﻿using System.ComponentModel.DataAnnotations;

namespace Models.Location
{
    public class Location
    {
        [Key]
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
