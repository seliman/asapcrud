import { BrowserModule } from '@angular/platform-browser';  
import { NgModule } from '@angular/core';  
  
import { AppComponent } from './app.component';  
import { FormsModule, ReactiveFormsModule } from '@angular/forms';  
import { HttpClientModule }    from '@angular/common/http';  
import { PersonService} from './services/person.service';  
import { LocationService} from './services/location.service';  
import { MatSelectModule} from '@angular/material/select';
import { MatToolbarModule} from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule ,routingComponents } from './app-routing.module';
@NgModule({
  declarations: [  
    AppComponent, routingComponents 
  ],  
  imports: [  
    BrowserModule,  
    FormsModule,  
    ReactiveFormsModule,  
    MatSelectModule,
    MatToolbarModule,
    AppRoutingModule,
    HttpClientModule, BrowserAnimationsModule  
  ],  
  providers: [PersonService, LocationService],  
  bootstrap: [AppComponent]  
})  
export class AppModule { }  
