import { Component, OnInit } from '@angular/core';
import {LocationService} from '../services/location.service';  
import { FormGroup, FormControl,Validators } from '@angular/forms';  
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.sass']
})
export class LocationComponent implements OnInit {

  constructor(private LocationService: LocationService) { }  
  data: any;  
  LocationForm: FormGroup;  
  submitted = false;   
  EventValue: any = "Save";  
  
  ngOnInit(): void {  
    this.getdata();  
    this.LocationForm = new FormGroup({  
      Id: new FormControl(null),  
      Name: new FormControl("",[Validators.required]),        
      Description: new FormControl("") 
    })    
  }  
  getdata() {  
    this.LocationService.getData().subscribe((data: any[]) => {  
       this.data = data;  
    })  
  }  
  deleteData(id) {  
    this.LocationService.deleteData(id).subscribe((data: any[]) => {  
      this.data = data;  
      this.getdata();  
    })  
  }  
  Save() {   
    this.submitted = true;  
    
     if (this.LocationForm.invalid) {  
            return;  
     }  
    this.LocationService.postData(this.LocationForm.value).subscribe((data: any[]) => {  
      this.data = data;  
      this.resetFrom();  
    })  
  }  
  Update() {   
    this.submitted = true;  
    
    if (this.LocationForm.invalid) {  
     return;  
    }        
    this.LocationService.putData(this.LocationForm.value.Id,this.LocationForm.value).subscribe((data: any[]) => {  
      this.data = data;  
      this.resetFrom();  
    })  
  }  
  
  EditData(Data) {  
    this.LocationForm.controls["Id"].setValue(Data.id);  
    this.LocationForm.controls["Name"].setValue(Data.name);      
    this.LocationForm.controls["Description"].setValue(Data.description);  
    this.EventValue = "Update";  
  }  
  
  resetFrom()  
  {     
    this.getdata();  
    this.LocationForm.reset();  
    this.EventValue = "Save";  
    this.submitted = false;   
  }
}
