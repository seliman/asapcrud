import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders }    from '@angular/common/http';  

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  constructor(private http: HttpClient) { }  
  httpOptions = {  
    headers: new HttpHeaders({  
      'Content-Type': 'application/json'  
    })  
  }    
  getData(){  
    return this.http.get('/api/Location');  //https://localhost:44352/ webapi host url  
  }  
  postData(formData){  
    return this.http.post('/api/Location',formData);  
  }  
  putData(id,formData){  
    return this.http.put('/api/Location/'+id,formData);  
  }  
  deleteData(id){  
    return this.http.delete('/api/Location/'+id);  
  }  
}
