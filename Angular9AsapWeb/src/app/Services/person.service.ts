import { Injectable } from '@angular/core';  
import { HttpClient,HttpHeaders }    from '@angular/common/http';  
@Injectable({  
  providedIn: 'root'  
})  
  
export class PersonService {  
  
constructor(private http: HttpClient) { }  
  httpOptions = {  
    headers: new HttpHeaders({  
      'Content-Type': 'application/json'  
    })  
  }    
  getData(){  
    return this.http.get('/api/Person');  //https://localhost:44352/ webapi host url  
  }  
  
  postData(formData){  
    return this.http.post('/api/Person',formData);  
  }  
  
  putData(id,formData){  
    return this.http.put('/api/Person/'+id,formData);  
  }  
  deleteData(id){  
    return this.http.delete('/api/Person/'+id);  
  }  
    
}  