import { Component, OnInit } from '@angular/core';
import {PersonService} from '../services/person.service';  
import {LocationService} from '../services/location.service';  
import { FormGroup, FormControl,Validators } from '@angular/forms';  
@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.sass']
})
export class PersonComponent implements OnInit {
  constructor(private PersonService: PersonService, private LocationService: LocationService) { }  
  data: any;  
  locationData: any;
  PersonForm: FormGroup;  
  submitted = false;   
  EventValue: any = "Save";  
  
  ngOnInit(): void {  
    this.getLocations();
    this.getdata();  
    this.PersonForm = new FormGroup({  
      Id: new FormControl(null),  
      Name: new FormControl("",[Validators.required]),        
      PhoneNumber: new FormControl("",[Validators.required]),  
      Email:new FormControl("",Validators.compose([Validators.email, Validators.required])), 
      DetailedAddress: new FormControl("",[Validators.required]),  
      LocationId:new FormControl(""), 
    })    
  }  
  getdata() {  
    this.PersonService.getData().subscribe((data: any[]) => {  
       this.data = data;  
    })  
  }  
  getLocations() {
    this.LocationService.getData().subscribe((data: any[]) => { 
      this.locationData = data.map(location => ({ value: location.id, text: location.name }));
    }) 
  }
  deleteData(id) {  
    this.PersonService.deleteData(id).subscribe((data: any[]) => {  
      this.data = data;  
      this.getdata();  
    })  
  }  
  Save() {   
    this.submitted = true;  
    
     if (this.PersonForm.invalid) {  
            return;  
     }  
    this.PersonService.postData(this.PersonForm.value).subscribe((data: any[]) => {  
      this.data = data;  
      this.resetFrom();  
    })  
  }  
  Update() {   
    this.submitted = true;  
    
    if (this.PersonForm.invalid) {  
     return;  
    }        
    this.PersonService.putData(this.PersonForm.value.Id,this.PersonForm.value).subscribe((data: any[]) => {  
      this.data = data;  
      this.resetFrom();  
    })  
  }  
  
  EditData(Data) {  
    this.PersonForm.controls["Id"].setValue(Data.id);  
    this.PersonForm.controls["Name"].setValue(Data.name);      
    this.PersonForm.controls["PhoneNumber"].setValue(Data.phoneNumber);  
    this.PersonForm.controls["Email"].setValue(Data.email);  
    this.PersonForm.controls["LocationId"].setValue(Data.locationId);  
    this.PersonForm.controls["DetailedAddress"].setValue(Data.detailedAddress);  
    this.EventValue = "Update";  
  }  
  
  resetFrom()  
  {     
    this.getdata();  
    this.PersonForm.reset();  
    this.EventValue = "Save";  
    this.submitted = false;   
  }  
}
